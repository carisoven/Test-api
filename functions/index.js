const functions = require("firebase-functions");
const admin = require("firebase-admin");

const app = require("express")();
admin.initializeApp();

const config = {
  apiKey: "AIzaSyCQOzxxF8RgP0jo7evookE7kk3XI4Hj31k",
  authDomain: "test-9fe50.firebaseapp.com",
  databaseURL: "https://test-9fe50.firebaseio.com",
  projectId: "test-9fe50",
  storageBucket: "test-9fe50.appspot.com",
  messagingSenderId: "411343414868",
  appId: "1:411343414868:web:7b7ecaaed009474d7cefc1",
  measurementId: "G-V58VFLXYWD"
};

const firebase = require("firebase");
firebase.initializeApp(config);

const db = admin.firestore();

app.get("/screams", (req, res) => {
  db.collection("screams")
    .get()
    .then(data => {
      let screams = [];
      data.forEach(doc => {
        screams.push(doc.data());
      });
      return res.json(screams);
    })
    .catch(err => console.error(err));
});

const FBAuth = (req, res, next) => {
  let idToken;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer ')
  ) {
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else {
    console.error('No token found');
    return res.status(403).json({ error: 'Unauthorized' });
  }

  admin
    .auth()
    .verifyIdToken(idToken)
    .then((decodedToken) => {
      req.user = decodedToken;
      return db
        .collection('user')
        .where('userId', '==', req.user.uid)
        .limit(1)
        .get();
    })
    .then((data) => {
      req.user.handle = data.docs[0].data().handle;
      req.user.imageUrl = data.docs[0].data().imageUrl;
      return next();
    })
    .catch((err) => {
      console.error('Error while verifying token ', err);
      return res.status(403).json(err);
    });
  };

app.post("/scream", FBAuth , (req, res) => {
  if (req.method !== "POST") {
    return res.status(400).json({ error: "method not allowed" });
  }
  const newScream = {
    body: req.body.body,
    userHandle: req.user.handle,
    createdAt: new Date().toISOString()
  };

  db.collection("screams")
    .add(newScream)
    .then(doc => {
      res.json({ massege: `document  ${doc.id} created successfully` });
    })
    .catch(err => {
      res.status(500).json({ error: "something went wrong" });
      console.error(err);
    });
});

const isEmail = email => {
  const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (email.match(regEx)) return true;
  else return false;
};

const isEmpty = string => {
  if (string.trim() === "") return true;
  else return false;
};

app.post("/signup", (req, res) => {
  const newUser = {
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body.confirmPassword,
    handle: req.body.handle
  };

  let errors = {};
  if (isEmpty(newUser.email)) {
    errors.email = "Must not be empty";
  } else if (!isEmail(newUser.email)) {
    errors.email = "Must be a valid email address";
  }

  if (isEmpty(newUser.password)) errors.password = "Must not be empty";
  if (newUser.password !== newUser.confirmPassword)
    errors.confirmPassword = "Password nust match";
  if (isEmpty(newUser.handle)) errors.handle = "Must not be empty";

  if (Object.keys(errors).length > 0) return res.status(400).json(errors);

  let token, userId;
  db.doc(`/users/${newUser.handle}`)
    .get()
    //สร้างเงื่อนไข โดยถูกกำหนดให้ถ้ามีโทเค็นแล้วให้แจ้งเตือน
    .then(doc => {
      if (doc.exists) {
        //แจ้งเตือน ส่งค่ากลับเป็น JSON
        return res.status(400).json({ handle: "this handle is already taken" });
      } else {
        //สร้าง Account โดยนำค่ามาจากตัวแปร
        return firebase
          .auth()
          .createUserWithEmailAndPassword(newUser.email, newUser.password);
      }
    })
    //รับข้อมูล Token
    .then(data => {
      userId = data.user.uid;
      return data.user.getIdToken();
    })
    //แสดง Token แบบ JSON
    .then(idToken => {
      token = idToken;
      const userCredentials = {
        handle: newUser.handle,
        email: newUser.email,
        createdAt: new Date().toISOString(),
        userId
      };
      return db.doc(`/user/${newUser.handle}`).set(userCredentials);
      //return res.status(201).json({ token });
    })
    .then(() => {
      return res.status(201).json({ token });
    })
    //ถ้าเงื่อนไขทั้งหมดมีปัญหาให้ทำตามคำสั่ง
    .catch(err => {
      //บันทึกลง Log
      console.error(err);
      //ถ้าเกิด Error โดยที่ User มีอีเมลแล้วให้ แสดงข้อความที่กำหนดถ้าไม่ไช่ให้แสดงข้อความตามCodeที่ Error
      if (err.code === "auth/email-already-in-use") {
        return res.status(400).json({ email: "Email is already is use" });
      } else {
        return res
          .status(500)
          .json({ general: "Something went wrong, please try again" });
      }
    });
});

app.post("/login", (req, res) => {
  const user = {
    email: req.body.email,
    password: req.body.password
  };

  let errors = {};

  if (isEmpty(user.email)) errors.email = "Must not be empty";
  if (isEmpty(user.password)) errors.password = "Must not be empty";
  if (Object.keys(errors).length > 0) return res.status(400).json(errors);

  firebase
    .auth()
    .signInWithEmailAndPassword(user.email, user.password)
    .then(data => {
      return data.user.getIdToken();
    })
    .then(token => {
      return res.json({ token });
    })
    .catch(err => {
      console.error(err);
      if (err.code === "auth/wrong-password") {
        return res
          .status(400)
          .json({ email: " Worng Email and Password Plesae to try again" });
      } else {
        return res
          .status(500)
          .json({ general: "Something went wrong, please try again" });
      }
      //return res.status(500).json({ error: err.code });
    });
});

exports.api = functions.https.onRequest(app);
